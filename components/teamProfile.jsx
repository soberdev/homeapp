import Image from "next/image";

const TeamProfile = () => {
  return (
    <div
      id="teamProfile"
      className="background2"
      style={{ position: "relative" }}
    >
      <Image
        src="/img/person.jpg"
        alt="person"
        width={1000}
        height={1000}
        layout="responsive"
      />
    </div>
  );
};

export default TeamProfile;
