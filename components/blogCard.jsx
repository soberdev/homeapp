import Image from "next/image";
import styles from "../styles/BlogCard.module.css";
import Button from "./button";

const BlogCard = () => {
  return (
    <div
      id="blogCard"
      className="background2 p-4"
      style={{ position: "relative", height: "25vw" }}
    >
      <h4 className={styles.containerText}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      </h4>
      <Button />
      <Image
        src="/img/blog1.png"
        alt="blog 1"
        layout="fill"
        objectFit="cover"
      />
    </div>
  );
};

export default BlogCard;
