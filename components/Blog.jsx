import Image from "next/image";
import styles from "../styles/Blog.module.css";
import BlogCard from "./blogCard";
import styled from "styled-components";

const Blog = () => {
  const BlogCard1 = styled(BlogCard)`
    height: 51vw;
  `;
  return (
    <div id="Blog" className={`${styles.container}`}>
      {/* section team */}
      <div
        className="background2 text-center p-5"
        style={{ position: "relative" }}
      >
        <h5 className={styles.containerText}>Soberdev</h5>
        <p className={styles.containerText}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tincidunt et
          eleifend sagittis, orci. At penatibus egestas id turpis. Eu velit
          tellus sit ultrices amet elit. Ut nulla posuere arcu bibendum leo. At
          leo urna varius risus et sit. Scelerisque fames sit eget pulvinar enim
          in interdum duis lorem. Rutrum nisi pulvinar lobortis lorem lacus,
          urna amet. Nisl, dolor diam vel egestas ut. Turpis et magna ridiculus
          ligula metus sed. Aliquet diam scelerisque sollicitudin libero et
          egestas. Donec nec morbi nam molestie consectetur quis. Auctor sed
          tellus quis pretium nisi purus.
        </p>
        <Image
          src="/img/background2.png"
          alt="background 2"
          layout="fill"
          objectFit="cover"
        />
      </div>
      <div className={`${styles.container} p-5`}>
        <div className="d-flex justify-content-between">
          <h3>Highlight</h3>
          <button type="button" className="btn btn-outline-secondary">
            Read More
          </button>
        </div>
        <div className="row pt-2 g-2">
          <div className={`${styles.container} col-md-8`}>
            <div className="row align-items-end g-2">
              <div className={`${styles.potrait} col-md-6`}>
                <BlogCard />
              </div>
              <div className={`${styles.potrait} col-md-6`}>
                <BlogCard />
              </div>
              <div className={`${styles.potrait} col`}>
                <BlogCard />
              </div>
            </div>
          </div>
          <div className={`${styles.container} col-md-4`}>
            <BlogCard1 />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Blog;
