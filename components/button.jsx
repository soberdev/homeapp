import styles from "../styles/Button.module.css";

const Button = () => {
  return (
    <button type="button" className={`${styles.btn} btn btn-outline-secondary`}>
      Read More
    </button>
  );
};

export default Button;
