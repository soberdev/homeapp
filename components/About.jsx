import Image from "next/image";
import styles from "../styles/About.module.css";
import TeamProfile from "./teamProfile";

const About = () => {
  return (
    <div id="About Us" className={`${styles.container}`}>
      <div id="about" className="row align-items-center p-5">
        <div className="container col-md-5 p-5">
          <Image
            src="/img/brain.png"
            alt="brain image"
            width={1300}
            height={1000}
            layout="responsive"
            className={styles.image}
          />
        </div>
        <div className={`${styles.column} col-md-7 text-center`}>
          <h5>About</h5>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tincidunt
            et eleifend sagittis, orci. At penatibus egestas id turpis. Eu velit
            tellus sit ultrices amet elit. Ut nulla posuere arcu bibendum leo.
            At leo urna varius risus et sit. Scelerisque fames sit eget pulvinar
            enim in interdum duis lorem. Rutrum nisi pulvinar lobortis lorem
            lacus, urna amet. Nisl, dolor diam vel egestas ut. Turpis et magna
            ridiculus ligula metus sed. Aliquet diam scelerisque sollicitudin
            libero et egestas. Donec nec morbi nam molestie consectetur quis.
            Auctor sed tellus quis pretium nisi purus.
          </p>
        </div>
      </div>

      {/* section team */}
      <div
        className="background2 text-center p-5"
        style={{ position: "relative" }}
      >
        <h5 className={styles.containerText}>Soberdev</h5>
        <p className={styles.containerText}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tincidunt et
          eleifend sagittis, orci. At penatibus egestas id turpis. Eu velit
          tellus sit ultrices amet elit. Ut nulla posuere arcu bibendum leo. At
          leo urna varius risus et sit. Scelerisque fames sit eget pulvinar enim
          in interdum duis lorem. Rutrum nisi pulvinar lobortis lorem lacus,
          urna amet. Nisl, dolor diam vel egestas ut. Turpis et magna ridiculus
          ligula metus sed. Aliquet diam scelerisque sollicitudin libero et
          egestas. Donec nec morbi nam molestie consectetur quis. Auctor sed
          tellus quis pretium nisi purus.
        </p>
        <Image
          src="/img/background2.png"
          alt="background 2"
          layout="fill"
          objectFit="cover"
        />
      </div>
      <div
        id="Our Team"
        className={`${styles.container} row align-items-center p-5`}
      >
        <div className={`${styles.column} col-md-6 text-center p-5`}>
          <h5>Meet Our Team!</h5>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tincidunt
            et eleifend sagittis, orci. At penatibus egestas id turpis. Eu velit
            tellus sit ultrices amet elit. Ut nulla posuere arcu bibendum leo.
            At leo urna varius risus et sit. Scelerisque fames sit eget pulvinar
            enim in interdum duis lorem. Rutrum nisi pulvinar lobortis lorem
            lacus, urna amet. Nisl, dolor diam vel egestas ut. Turpis et magna
            ridiculus ligula metus sed. Aliquet diam scelerisque sollicitudin
            libero et egestas. Donec nec morbi nam molestie consectetur quis.
            Auctor sed tellus quis pretium nisi purus.
          </p>
        </div>
        <div className={`${styles.container} col-md-6 text-center p-5`}>
          <h3>Our Team</h3>
          <div className="row align-items-center g-2">
            <div className={`${styles.potrait} col-md-4`}>
              <TeamProfile />
            </div>
            <div className={`${styles.potrait} col-md-4`}>
              <TeamProfile />
            </div>
            <div className={`${styles.potrait} col-md-4`}>
              <TeamProfile />
            </div>
            <div className={`${styles.potrait} col-md-4`}>
              <TeamProfile />
            </div>
            <div className={`${styles.potrait} col-md-4`}>
              <TeamProfile />
            </div>
            <div className={`${styles.potrait} col-md-4`}>
              <TeamProfile />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
