import Image from "next/image";
import styles from "../styles/Navbar.module.css";

const Navbar = () => {
  return (
    <div className={`${styles.container} row px-5 py-3 `}>
      <div className={`${styles.itemLeft} col-md-8`}>
        <div className={styles.callButton}>
          <Image
            src="/img/logo-white.png"
            alt=""
            width="113"
            height="35"
            className={styles.imgLogo}
          />
        </div>
      </div>
      <div className={`${styles.itemRight} col-md-4 align-items-center`}>
        <ul className={`${styles.list} align-items-center`}>
          <li className={styles.listItem}>
            <a href="#About Us" className="nav-link">
              About Us
            </a>
          </li>
          <li className={styles.listItem}>
            <a href="#Our Team" className="nav-link">
              Our Team
            </a>
          </li>
          <li className={styles.listItem}>
            <a href="#Blog" className="nav-link">
              Blog
            </a>
          </li>
          <li className={styles.listItem}>
            <a href="#Contact Us" className="nav-link">
              Contact Us
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Navbar;
