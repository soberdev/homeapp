import Image from "next/image";
import styles from "../styles/Footer.module.css";

const Footer = () => {
  return (
    <div id="Contact Us" className={`${styles.container} px-5`}>
      <div className="p-5">
        <div className={`${styles.logo}`}>
          <Image
            src="/img/logo-white.png"
            alt="logo white"
            width="113"
            height="35"
            className={styles.imgLogo}
          />
        </div>
        <h3>Info</h3>
        <h5>0812-3456-7890</h5>
        <h5>info@soberdev</h5>
        <h3>Address</h3>
        <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5>
      </div>
    </div>
  );
};

export default Footer;
