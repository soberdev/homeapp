import Navbar from "./Navbar"
import Footer from "./Footer"
import Hero from "./Hero"
import About from "./About"
import Blog from "./Blog"

const Layout = ({children}) => {
    return (
        <>
            {/* <Navbar/> */}
            <Hero/>
            <About/>
            <Blog/>
            {children}
            <Footer/>
        </>
    )
}

export default Layout