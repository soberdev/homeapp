import Image from "next/image";
import styles from "../styles/Hero.module.css";
import Navbar from "./Navbar";

const Hero = () => {
  return (
    <>
      <div
        id="Hero"
        className="row background2 px-5"
        style={{ position: "relative" }}
      >
        <Navbar />
        <div className="row align-items-center px-5">
          <div className={`${styles.column} col-md-6 text-center p-5`}>
            <h5 className={styles.containerText}>Moving your next level</h5>
            <h1 className={styles.containerText}>The Magic of Change</h1>
            <p className={styles.containerText}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Facilisis
              scelerisque sagittis suspendisse
            </p>
          </div>
          <div className="col-md-6 p-5">
            <Image
              src="/img/idea.png"
              alt="idea"
              width={1000}
              height={1400}
              layout="responsive"
              className={styles.imgContainer}
            />
          </div>
        </div>

        <Image
          src="/img/background.png"
          alt="background"
          layout="fill"
          objectFit="cover"
        />
      </div>
    </>
  );
};

export default Hero;
